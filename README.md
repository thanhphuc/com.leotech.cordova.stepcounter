com.leotech.cordova.stepcounter (iOS only)
==========================================

This is an update of original plugin to get `numberOfSteps` from iOS Health Application. Updated to run on iOS 8+

Available Functions
-------------------

```
window.cordova.plugins.StepCounter.isSupported(function(response) {
    console.log(response); //boolean
});
```

```
window.cordova.plugins.StepCounter.getData(from, to, function(response) {
    console.log(response.numberOfSteps); //number
});
```